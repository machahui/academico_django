# -*- coding: utf-8 -*-

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import ResPartner,AcademicRegistroNotasProfesor,ResUsers,HrEmployee
admin.site.register(ResPartner)
admin.site.register(AcademicRegistroNotasProfesor)
admin.site.register(ResUsers)
admin.site.register(HrEmployee)
# Register your models here.
