# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from datetime import date
from django.db import connection

class ResUsers(models.Model):
    # duser = models.OneToOneField(User, on_delete=models.CASCADE)
    duser_id = models.IntegerField()
    login = models.CharField(max_length=64)
    password_crypt = models.CharField(max_length=200)
    active = models.BinaryField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'res_users'

    def __str__(self):
        return '%s' % (self.login)

    def docente(self):
        return HrEmployee.objects.get(resource__user_id=self.id)

    def actualizar_usuario(self):
        # user = self.duser
        if not self.password_crypt or not self.login:
            return False
        if not self.duser_id:
            user = User(username = self.login, password = self.password_crypt, is_staff= 1, is_active = 1)
            user.save()
            with connection.cursor() as cursor:
                cursor.execute('update res_users set duser_id = %s where id = %s',
                               [user.id, self.id])

class ResPartner(models.Model):
    name = models.TextField(null=True,blank=True)
    first_name = models.TextField(null=True,blank=True)
    last_name_f = models.TextField(null=True,blank=True)
    last_name_m = models.TextField(null=True,blank=True)
    employee = models.BinaryField(blank=True, null=True)
    is_student = models.BinaryField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'res_partner'

    def __str__(self):
        return '%s %s, %s' % (self.last_name_f,self.last_name_m,self.first_name)

class ResourceResource(models.Model):
    user = models.ForeignKey(ResUsers, models.DO_NOTHING)
    name = models.TextField(null=True,blank=True)

    class Meta:
        managed = False
        db_table = 'resource_resource'

class HrEmployee(models.Model):
    # address = models.ForeignKey(ResPartner, models.DO_NOTHING, blank=True, null=True)
    create_date = models.DateTimeField(blank=True, null=True)
    coach = models.ForeignKey('self', models.DO_NOTHING, blank=True, null=True)
    resource = models.ForeignKey(ResourceResource, models.DO_NOTHING)
    color = models.IntegerField(blank=True, null=True)
    message_last_post = models.DateTimeField(blank=True, null=True)
    image = models.BinaryField(blank=True, null=True)
    marital = models.CharField(max_length=300, blank=True, null=True)
    identification_id = models.CharField(max_length=200, blank=True, null=True)
    # bank_account = models.ForeignKey('ResPartnerBank', models.DO_NOTHING, blank=True, null=True)
    job = models.ForeignKey('HrJob', models.DO_NOTHING, blank=True, null=True)
    work_phone = models.CharField(max_length=200, blank=True, null=True)
    # country = models.ForeignKey('ResCountry', models.DO_NOTHING, blank=True, null=True)
    # parent = models.ForeignKey('self', models.DO_NOTHING, blank=True, null=True)
    # department = models.ForeignKey(HrDepartment, models.DO_NOTHING, blank=True, null=True)
    otherid = models.CharField(max_length=200, blank=True, null=True)
    mobile_phone = models.CharField(max_length=200, blank=True, null=True)
    # create_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='create_uid', blank=True, null=True)
    birthday = models.DateField(blank=True, null=True)
    # write_date = models.DateTimeField(blank=True, null=True)
    sinid = models.CharField(max_length=200, blank=True, null=True)
#     # write_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='write_uid', blank=True, null=True)
    work_email = models.CharField(max_length=240, blank=True, null=True)
    work_location = models.CharField(max_length=200, blank=True, null=True)
    image_medium = models.BinaryField(blank=True, null=True)
    gender = models.CharField(max_length=200, blank=True, null=True)
    ssnid = models.CharField(max_length=200, blank=True, null=True)
    image_small = models.BinaryField(blank=True, null=True)
    # address_home = models.ForeignKey(ResPartner, models.DO_NOTHING, blank=True, null=True)
    passport_id = models.CharField(max_length=200, blank=True, null=True)
    name_related = models.CharField(max_length=200, blank=True, null=True)
    notes = models.TextField(blank=True, null=True)
    es_docente = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'hr_employee'

    def __str__(self):
        return '%s' % (self.name_related)

class HrJob(models.Model):
    create_date = models.DateTimeField(blank=True, null=True)
#     # write_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='write_uid', blank=True, null=True)
    # create_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='create_uid', blank=True, null=True)
    requirements = models.TextField(blank=True, null=True)
    message_last_post = models.DateTimeField(blank=True, null=True)
    # company = models.ForeignKey('ResCompany', models.DO_NOTHING, blank=True, null=True)
    state = models.CharField(max_length=25)
    expected_employees = models.IntegerField(blank=True, null=True)
    # department = models.ForeignKey(HrDepartment, models.DO_NOTHING, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    write_date = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=400)
    no_of_recruitment = models.IntegerField(blank=True, null=True)
    no_of_hired_employee = models.IntegerField(blank=True, null=True)
    no_of_employee = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'hr_job'
        # unique_together = (('name', 'company', 'department'),)

class AcademicCycle(models.Model):
    create_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='create_uid', blank=True, null=True)
    create_date = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=30, blank=True, null=True)
    # write_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='write_uid', blank=True, null=True)
    write_date = models.DateTimeField(blank=True, null=True)
    year_moved0 = models.CharField(max_length=200, blank=True, null=True)
    cycle = models.IntegerField(blank=True, null=True)
    fecha_entrega_subsa = models.DateField(blank=True, null=True)
    plan_curricular = models.ForeignKey('AcademicPlanCurricular', models.DO_NOTHING, db_column='plan_curricular', blank=True, null=True)
    message_last_post = models.DateTimeField(blank=True, null=True)
    plazo_entrega_nota = models.IntegerField(blank=True, null=True)
    fecha_entrega_1 = models.DateField(blank=True, null=True)
    fecha_entrega_2 = models.DateField(blank=True, null=True)
    fecha_entrega_3 = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'academic_cycle'

    def __str__(self):
        return '%s' % (self.name)

class AcademicPlanCurricular(models.Model):
    fin_vigencia = models.DateField(blank=True, null=True)
    code = models.CharField(max_length=10, blank=True, null=True)
    create_date = models.DateTimeField(blank=True, null=True)
    tipo_plan = models.CharField(max_length=300, blank=True, null=True)
    create_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='create_uid', blank=True, null=True)
    name = models.CharField(max_length=300, blank=True, null=True)
    ini_vigencia = models.DateField(blank=True, null=True)
    resolucion = models.CharField(max_length=200, blank=True, null=True)
    message_last_post = models.DateTimeField(blank=True, null=True)
    # write_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='write_uid', blank=True, null=True)
    active = models.NullBooleanField()
    state = models.CharField(max_length=200, blank=True, null=True)
    write_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'academic_plan_curricular'

    def __str__(self):
        return '%s' % (self.name)

class AcademicUbicacion(models.Model):
    # create_uid = models.ForeignKey('ResUsers', models.DO_NOTHING, db_column='create_uid', blank=True, null=True)
    create_date = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    # write_uid = models.ForeignKey('ResUsers', models.DO_NOTHING, db_column='write_uid', blank=True, null=True)
    write_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'academic_ubicacion'

class AcademicSeccion(models.Model):
    capacidad = models.IntegerField(blank=True, null=True)
    codigo = models.CharField(max_length=200, blank=True, null=True)
    create_date = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    create_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='create_uid', blank=True, null=True)
    # write_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='write_uid', blank=True, null=True)
    write_date = models.DateTimeField(blank=True, null=True)
    ubicacion = models.ForeignKey(AcademicUbicacion, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'academic_seccion'

    def __str__(self):
        return '%s' % (self.name)

class AcademicPeriods(models.Model):
    create_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='create_uid', blank=True, null=True)
    create_date = models.DateTimeField(blank=True, null=True)
    name = models.IntegerField(blank=True, null=True)
    # write_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='write_uid', blank=True, null=True)
    write_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'academic_periods'

    def __str__(self):
        return '%s' % (self.name)

class ApcUnidadDidactica(models.Model):
    # create_uid = models.ForeignKey('ResUsers', models.DO_NOTHING, db_column='create_uid', blank=True, null=True)
    create_date = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=350, blank=True, null=True)
    # write_uid = models.ForeignKey('ResUsers', models.DO_NOTHING, db_column='write_uid', blank=True, null=True)
    write_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'apc_unidad_didactica'

    def __str__(self):
        return '%s' % (self.name)

class AcademicPlanCurricularLinea(models.Model):
    create_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='create_uid', blank=True, null=True)
    create_date = models.DateTimeField(blank=True, null=True)
    creditos = models.FloatField(blank=True, null=True)
    modulo = models.ForeignKey('ApcModulo', models.DO_NOTHING, blank=True, null=True)
    apc = models.ForeignKey(AcademicPlanCurricular, models.DO_NOTHING, blank=True, null=True)
    # write_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='write_uid', blank=True, null=True)
    active = models.NullBooleanField()
    unidad = models.ForeignKey(ApcUnidadDidactica, models.DO_NOTHING, blank=True, null=True)
    period = models.ForeignKey(AcademicPeriods, models.DO_NOTHING, blank=True, null=True)
    write_date = models.DateTimeField(blank=True, null=True)
    carrera = models.ForeignKey('ApcCarrera', models.DO_NOTHING, blank=True, null=True)
    horas = models.IntegerField(blank=True, null=True)
    tipo_plan = models.CharField(max_length=300, blank=True, null=True)
    ordenamiento = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'academic_plan_curricular_linea'

    def __str__(self):
        return '%s %s %s %s' % (self.apc.code,self.carrera.code,self.period,self.unidad)

class AcademicSeccionCursoPc(models.Model):
    create_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='create_uid', blank=True, null=True)
    create_date = models.DateTimeField(blank=True, null=True)
    orden = models.CharField(max_length=200, blank=True, null=True)
    tp_turno = models.CharField(max_length=200, blank=True, null=True)
    seccion = models.ForeignKey(AcademicSeccion, models.DO_NOTHING, blank=True, null=True)
    # write_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='write_uid', blank=True, null=True)
    ciclo = models.ForeignKey(AcademicCycle, models.DO_NOTHING, blank=True, null=True)
    write_date = models.DateTimeField(blank=True, null=True)
    unidades_didacticas = models.ForeignKey(AcademicPlanCurricularLinea, models.DO_NOTHING, db_column='unidades_didacticas', blank=True, null=True)
    # asistencia_cabezeras = models.ForeignKey(AcademicAsistenciaCabecera, models.DO_NOTHING, db_column='asistencia_cabezeras', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'academic_seccion_curso_pc'

    def __str__(self):
        return '%s %s' % (self.unidades_didacticas ,self.tp_turno)

class AcademicAsistenciaCabecera(models.Model):
    curso = models.ForeignKey(AcademicSeccionCursoPc, models.DO_NOTHING, blank=True, null=True)
    create_date = models.DateTimeField(blank=True, null=True)
    create_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='create_uid', blank=True, null=True)
    check_academico = models.NullBooleanField()
    message_last_post = models.DateTimeField(blank=True, null=True)
    # write_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='write_uid', blank=True, null=True)
    ciclo = models.ForeignKey(AcademicCycle, models.DO_NOTHING, blank=True, null=True)
    state = models.CharField(max_length=200, blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    write_date = models.DateTimeField(blank=True, null=True)
    aviso = models.TextField(blank=True, null=True)
    hora_ini = models.CharField(max_length=200, blank=True, null=True)
    docente = models.ForeignKey(HrEmployee, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'academic_asistencia_cabecera'

class AcademicRegistroNotasProfesor(models.Model):
    curso = models.ForeignKey(AcademicSeccionCursoPc, models.DO_NOTHING, blank=True, null=True)
    create_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='create_uid', blank=True, null=True)
    docente = models.ForeignKey(HrEmployee, models.DO_NOTHING, blank=True, null=True)
    message_last_post = models.DateTimeField(blank=True, null=True)
    # write_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='write_uid', blank=True, null=True)
    ciclo = models.ForeignKey(AcademicCycle, models.DO_NOTHING, blank=True, null=True)
    date_n4 = models.DateField(blank=True, null=True)
    write_date = models.DateTimeField(blank=True, null=True)
    date_n3 = models.DateField(blank=True, null=True)
    date_n2 = models.DateField(blank=True, null=True)
    create_date = models.DateTimeField(blank=True, null=True)
    date_n1 = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'academic_registro_notas_profesor'

    def __str__(self):
        return '%s %s %s %s' % (self.docente,self.ciclo,self.curso,self.state)

    def academic_registo_matricula(self):
        return  AcademicRegistroMatricula.objects.filter(registro_notas=self.id).order_by('alumno__last_name_f')

    def state(self):
        state = 'pendiente'
        if self.academic_registo_matricula() and self.academic_registo_matricula()[0].matricula.estado_final:
            state = 'terminado'
        return  state

class ApcCarrera(models.Model):
    create_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='create_uid', blank=True, null=True)
    code = models.CharField(max_length=10, blank=True, null=True)
    create_date = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=350, blank=True, null=True)
    # write_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='write_uid', blank=True, null=True)
    write_date = models.DateTimeField(blank=True, null=True)
    tipo_carrera = models.CharField(max_length=200, blank=True, null=True)
    # secuencia = models.ForeignKey('IrSequence', models.DO_NOTHING, db_column='secuencia', blank=True, null=True)
    codigo = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'apc_carrera'

    def __str__(self):
        return '%s' % (self.name)

class ApcModulo(models.Model):
    create_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='create_uid', blank=True, null=True)
    create_date = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=300, blank=True, null=True)
    # write_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='write_uid', blank=True, null=True)
    write_date = models.DateTimeField(blank=True, null=True)
    carrera = models.ForeignKey(ApcCarrera, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'apc_modulo'

class AcademicNomina(models.Model):
    create_date = models.DateTimeField(blank=True, null=True)
    modulo = models.ForeignKey(ApcModulo, models.DO_NOTHING, blank=True, null=True)
    periodo = models.ForeignKey(AcademicPeriods, models.DO_NOTHING, blank=True, null=True)
    # write_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='write_uid', blank=True, null=True)
    create_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='create_uid', blank=True, null=True)
    message_last_post = models.DateTimeField(blank=True, null=True)
    seccion = models.ForeignKey(AcademicSeccion, models.DO_NOTHING, blank=True, null=True)
    state = models.CharField(max_length=200, blank=True, null=True)
    # dato_g = models.ForeignKey(AcademicDatoGobierno, models.DO_NOTHING, db_column='dato_g', blank=True, null=True)
    carrera = models.ForeignKey(ApcCarrera, models.DO_NOTHING, blank=True, null=True)
    # secretario_academico = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='secretario_academico', blank=True, null=True)
    fecha_nomina = models.DateField(blank=True, null=True)
    ciclo = models.ForeignKey(AcademicCycle, models.DO_NOTHING, blank=True, null=True)
    # firme_general = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='firme_general', blank=True, null=True)
    write_date = models.DateTimeField(blank=True, null=True)
    turno_id = models.CharField(max_length=200, blank=True, null=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    fecha_envio = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'academic_nomina'


class AcademicMatricula(models.Model):
    turno_id = models.CharField(max_length=200, blank=True, null=True)
    create_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='create_uid', blank=True, null=True)
    create_date = models.DateTimeField(blank=True, null=True)
    active = models.NullBooleanField()
    modulo = models.ForeignKey(ApcModulo, models.DO_NOTHING, blank=True, null=True)
    periodo = models.ForeignKey(AcademicPeriods, models.DO_NOTHING, blank=True, null=True)
    seccion = models.ForeignKey(AcademicSeccion, models.DO_NOTHING, blank=True, null=True)
    # write_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='write_uid', blank=True, null=True)
    ciclo = models.ForeignKey(AcademicCycle, models.DO_NOTHING, blank=True, null=True)
    # sale_order = models.ForeignKey('SaleOrder', models.DO_NOTHING, blank=True, null=True)
    observacion = models.CharField(max_length=200, blank=True, null=True)
    write_date = models.DateTimeField(blank=True, null=True)
    nomina = models.ForeignKey(AcademicNomina, models.DO_NOTHING, blank=True, null=True)
    carrera = models.ForeignKey(ApcCarrera, models.DO_NOTHING, blank=True, null=True)
    plan = models.ForeignKey(AcademicPlanCurricular, models.DO_NOTHING, blank=True, null=True)
    estado_final = models.CharField(max_length=200, blank=True, null=True)
    partner = models.ForeignKey(ResPartner, models.DO_NOTHING, blank=True, null=True)
    mano = models.NullBooleanField()
    is_validation = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'academic_matricula'

def validate_even(value):
    if not value.isdigit():
        raise ValidationError(
            _('%(value)s is not an even number'),
            params={'value': value},
        )

class AcademicRegistroMatricula(models.Model):
    create_date = models.DateTimeField(blank=True, null=True)
    # write_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='write_uid', blank=True, null=True)
    nota_subsa = models.DecimalField(max_digits=65535, decimal_places=2, blank=True, null=True)
    nota_subsa_s = models.CharField(max_length=200, blank=True, null=True)
    nota_3 = models.DecimalField(max_digits=65535, decimal_places=2, blank=True, null=True)
    nota_2 = models.DecimalField(max_digits=65535, decimal_places=2, blank=True, null=True)
    nota_1 = models.DecimalField(max_digits=65535, decimal_places=2, blank=True, null=True,validators=[validate_even])
    registro_notas = models.ForeignKey(AcademicRegistroNotasProfesor, models.DO_NOTHING, db_column='registro_notas', blank=True, null=True)
    plan_curricular_detalle = models.ForeignKey(AcademicPlanCurricularLinea, models.DO_NOTHING, db_column='plan_curricular_detalle', blank=True, null=True)
    nota_prom = models.DecimalField(max_digits=65535, decimal_places=2, blank=True, null=True)
    nota_1_s = models.CharField(max_length=200, blank=True, null=True)
    message_last_post = models.DateTimeField(blank=True, null=True)
    nota_3_s = models.CharField(max_length=200, blank=True, null=True)
    nota_2_s = models.CharField(max_length=200, blank=True, null=True)
    matricula = models.ForeignKey(AcademicMatricula, models.DO_NOTHING, blank=True, null=True)
    tp_extraordinario = models.CharField(max_length=200, blank=True, null=True)
    create_uid = models.ForeignKey(ResUsers, models.DO_NOTHING, db_column='create_uid', blank=True, null=True)
    condicion_academica = models.CharField(max_length=200, blank=True, null=True)
    # sale_order = models.ForeignKey('SaleOrder', models.DO_NOTHING, blank=True, null=True)
    write_date = models.DateTimeField(blank=True, null=True)
    active = models.NullBooleanField()
    fecha_nota1 = models.DateField(blank=True, null=True)
    fecha_nota3 = models.DateField(blank=True, null=True)
    fecha_nota2 = models.DateField(blank=True, null=True)
    fecha_notasubsa = models.DateField(blank=True, null=True)
    tp_evaluacion = models.CharField(max_length=200, blank=True, null=True)
    nota_final = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    # original = models.ForeignKey('self', models.DO_NOTHING, db_column='original', blank=True, null=True)
    alumno = models.ForeignKey(ResPartner, models.DO_NOTHING, db_column='alumno', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'academic_registro_matricula'

    def es_editable_nota1(self):
        state = False
        if not self.fecha_nota1:
            state = True
        return state

    def es_editable_nota2(self):
        state = False
        if not self.fecha_nota2:
            state = True
        return state

    def es_editable_nota3(self):
        state = False
        if not self.fecha_nota3:
            state = True
        return state

    def es_editable_notasubsa(self):
        state = False
        if not self.fecha_notasubsa:
            state = True
        return state
