# -*- coding: utf-8 -*-

from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from .models import *
from django.http import Http404
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.http import JsonResponse
from django.db import connection

def completar_curso(request):
    ciclo_id = request.GET.get('ciclo_id')
    docente_id = request.GET.get('docente_id')
    if ciclo_id and docente_id:
        with connection.cursor() as cursor:
            query = """select distinct(d.id) as curso_id
                        from academic_docente_disponibilidad 			a
                             join academic_plan_periodo_horario			b on b.plan_periodo_id = a.id
                             join rel_plan_periodo_curso			c on b.id = c.periodo_horario_id
                             join academic_seccion_curso_pc			d on c.curso_id = d.id
                        where a.docente_id = %s and a.ciclo_id = %s"""
            cursor.execute(query,[docente_id,ciclo_id])
            cursos = cursor.fetchall()
            ascps_ids= [k[0] for k in cursos]
            cursos = AcademicSeccionCursoPc.objects.filter(id__in=ascps_ids)
            options = ''
            import sys
            reload(sys)
            sys.setdefaultencoding('utf8')
            for curso in cursos:
                options += "<option value='"+str(curso.id)+"'>"+str(curso)+"</option>"
            return JsonResponse(options,safe=False)

def usuario_nuevo(request):
    if request.method=='POST':
        formulario = UserCreationForm(request.POST)
        if formulario.is_valid:
            formulario.save()
            return HttpResponseRedirect('/')
    else:
        formulario = UserCreationForm()
    context = {'formulario': formulario}
    return render(request, 'nuevousuario.html', context)

def ingresar(request):
    if not request.user.is_anonymous():
        return HttpResponseRedirect('/privado')
    if request.method == 'POST':
        formulario = AuthenticationForm(request.POST)
        if formulario.is_valid:
            usuario = request.POST['username']
            clave = request.POST['password']
            acceso = authenticate(username=usuario, password=clave)
            if acceso is not None:
                if acceso.is_active:
                    login(request, acceso)
                    return HttpResponseRedirect('/privado')
                else:
                    return render(request, 'noactivo.html')
            else:
                return render(request, 'nousuario.html')
    else:
        formulario = AuthenticationForm()
    context = {'formulario': formulario}
    return render(request, 'ingresar.html', context)

def inicio(request):
    # recetas = Receta.objects.all()
    # context = {'recetas': }
    return render(request, 'inicio.html')

@login_required(login_url='/ingresar')
def privado(request):
    usuario = request.user
    context = {'usuario': usuario}
    return render(request, 'privado.html', context)

@login_required(login_url='/ingresar')
def cerrar(request):
    logout(request)
    return HttpResponseRedirect('/')

@login_required(login_url='/ingresar')
def academic_registro_notas_profesor(request):
    # usuarios = ResUsers.objects.all()
    # for usuario in usuarios:
    #     usuario.actualizar_usuario()

    user_odoo = ResUsers.objects.get(duser_id=request.user.id)
    if request.user.is_superuser:
        object_list = AcademicRegistroNotasProfesor.objects.all().order_by('-id')
    elif user_odoo.docente():
        object_list = AcademicRegistroNotasProfesor.objects.filter(docente_id=user_odoo.docente().id).order_by('-id')
    else:
        object_list = {}
    paginator = Paginator(object_list, 25)  # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        list_page = paginator.page(page)
    except PageNotAnInteger:
        list_page = paginator.page(1)
    except EmptyPage:
        list_page = paginator.page(paginator.num_pages)
    import sys
    reload(sys)
    sys.setdefaultencoding('utf8')
    context = {'lists': list_page,'usuario':request.user}

    return render(request, 'academic_registro_notas_profesor/index.html',context)

@login_required(login_url='/ingresar')
def academic_registro_notas_profesor_show(request,id):
    try:
        academic_registro_notas_profesor = AcademicRegistroNotasProfesor.objects.get(pk=id)
    except AcademicRegistroNotasProfesor.DoesNotExist:
        raise Http404('No existe registro')

    import sys
    reload(sys)
    sys.setdefaultencoding('utf8')
    context = {'academic_registro_notas_profesor': academic_registro_notas_profesor, 'usuario': request.user}
    return render(request, 'academic_registro_notas_profesor/show.html',context)

@login_required(login_url='/ingresar')
def academic_registro_notas_profesor_new(request):
    docente = ResUsers.objects.get(duser_id=request.user.id).docente()
    ciclos = AcademicCycle.objects.all().order_by('-name')
    ascps = AcademicSeccionCursoPc.objects.all()
    import sys
    reload(sys)
    sys.setdefaultencoding('utf8')
    context = {'docente':docente,'ciclos':ciclos,'ascps':ascps}
    return render(request, 'academic_registro_notas_profesor/new.html',context)

@login_required(login_url='/ingresar')
def academic_registro_notas_profesor_create(request):
    docente = request.POST['academic_registro_notas_profesor_docente']
    ciclo = request.POST['academic_registro_notas_profesor_ciclo']
    curso = request.POST['academic_registro_notas_profesor_ascps']
    object_list = AcademicRegistroNotasProfesor.objects.get(docente_id=docente,ciclo_id=ciclo,curso_id=curso)
    if not object_list:
        object_list = AcademicRegistroNotasProfesor(docente_id=docente,ciclo_id=ciclo,curso_id=curso)
        object_list.save()
        curso_model = AcademicSeccionCursoPc.objects.get(id=curso)
        notas_matricula = AcademicRegistroMatricula.objects.filter(matricula__estado_final__isnull=True,plan_curricular_detalle=curso_model.unidades_didacticas.id,matricula__ciclo_id=curso_model.ciclo_id,matricula__seccion_id=curso_model.seccion_id)
        for nota in notas_matricula:
            if not nota.registro_notas:
                with connection.cursor() as cursor:

                    cursor.execute('update academic_registro_matricula set registro_notas = %s where id = %s', [object_list.id, nota.id])
                # nota.registro_notas = object_list.id
                # nota.save()
    return HttpResponseRedirect(
        reverse('academic_registro_notas_profesor_show', args=(object_list.id,)))

@login_required(login_url='/ingresar')
def academic_registo_matricula_edit(request,id,unidad):
    try:
        nota = 0
        nota_s = ''
        academic_registro_matricula = AcademicRegistroMatricula.objects.get(pk=id)
        if unidad == '1':
            nota = academic_registro_matricula.nota_1
            nota_s = academic_registro_matricula.nota_1_s
        elif unidad == '2':
            nota = academic_registro_matricula.nota_2
            nota_s = academic_registro_matricula.nota_2_s
        elif unidad == '3':
            nota = academic_registro_matricula.nota_3
            nota_s = academic_registro_matricula.nota_3_s
        elif unidad == '4':
            nota = academic_registro_matricula.nota_subsa
            nota_s = academic_registro_matricula.nota_subsa_s
    except AcademicRegistroMatricula.DoesNotExist:
        raise Http404('No existe registro')
    context = {'academic_registro_matricula': academic_registro_matricula, 'usuario': request.user,'unidad':unidad,'nota':nota,'nota_s':nota_s}
    return render(request, 'academic_registo_matricula/edit.html',context)

@login_required(login_url='/ingresar')
def academic_registo_matricula_update(request,id):
    user_odoo = ResUsers.objects.get(duser_id=request.user.id)
    academic_registro_matricula = AcademicRegistroMatricula.objects.get(pk=id)
    nota = request.POST['academic_registo_matricula_nota']
    nota_s = request.POST['academic_registo_matricula_obs']
    if academic_registro_matricula and academic_registro_matricula.registro_notas and academic_registro_matricula.registro_notas.docente and user_odoo.docente().id == academic_registro_matricula.registro_notas.docente.id:
        if request.POST['academic_registo_matricula_unidad'] == '1':
            academic_registro_matricula.nota_1 = nota
            academic_registro_matricula.nota_1_s = nota_s
            academic_registro_matricula.fecha_nota1 = date.today()
        elif request.POST['academic_registo_matricula_unidad'] == '2':
            academic_registro_matricula.nota_2 = nota
            academic_registro_matricula.nota_2_s = nota_s
            academic_registro_matricula.fecha_nota2 = date.today()
        elif request.POST['academic_registo_matricula_unidad'] == '3':
            academic_registro_matricula.nota_3 = nota
            academic_registro_matricula.nota_3_s = nota_s
            academic_registro_matricula.fecha_nota3 = date.today()
        elif request.POST['academic_registo_matricula_unidad'] == '4':
            academic_registro_matricula.nota_subsa = nota
            academic_registro_matricula.nota_subsa_s = nota_s
            academic_registro_matricula.fecha_notasubsa = date.today()
        academic_registro_matricula.save()
        return HttpResponseRedirect(
            reverse('academic_registro_notas_profesor_show', args=(academic_registro_matricula.registro_notas.id,)))
    else:
        return HttpResponseRedirect(
            reverse('academic_registro_notas_profesor'))


def error_404(request):
        data = {}
        return render(request, 'academic/404.html', data)

def error_500(request):
    data = {}
    return render(request, 'academic/500.html', data)