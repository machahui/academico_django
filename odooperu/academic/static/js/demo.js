$("#academic_registro_notas_profesor_ciclo").change(function () {
      var ciclo_id = $(this).val();
      var docente_id = $('#academic_registro_notas_profesor_docente').val();
      $.ajax({
        url: '/ajax/completar_curso/',
        type: 'get',
        data: {
          'ciclo_id': ciclo_id,'docente_id':docente_id
        },
        dataType: 'text',
        success: function (data) {
            $('#academic_registro_notas_profesor_ascps').html(data)
        }
      });
    });
