from django.conf.urls import url
from django.conf.urls import handler404, handler500
from . import views

urlpatterns = [
    url(r'^$', views.inicio, name='inicio'),
    url(r'^usuario/nuevo/$', views.usuario_nuevo, name='usuario_nuevo'),
    url(r'^ingresar/$',views.ingresar,name='ingresar'),
    url(r'^privado', views.privado, name='privado'),
    url(r'^cerrar/$', views.cerrar, name='cerrar'),
    url(r'^academic_registro_notas_profesor/$', views.academic_registro_notas_profesor, name='academic_registro_notas_profesor'),
    url(r'^academic_registro_notas_profesor/new/$', views.academic_registro_notas_profesor_new, name='academic_registro_notas_profesor_new'),
    url(r'^academic_registro_notas_profesor/create/$', views.academic_registro_notas_profesor_create, name='academic_registro_notas_profesor_create'),
    url(r'^academic_registro_notas_profesor/(?P<id>[0-9]+)/$', views.academic_registro_notas_profesor_show, name='academic_registro_notas_profesor_show'),
    url(r'^academic_registo_matricula/(?P<id>[0-9]+)/(?P<unidad>[0-9]+)/edit/$', views.academic_registo_matricula_edit, name='academic_registo_matricula_edit'),
    url(r'^academic_registo_matricula/(?P<id>[0-9]+)/update/$', views.academic_registo_matricula_update, name='academic_registo_matricula_update'),
    url(r'^ajax/completar_curso/$', views.completar_curso, name='completar_curso'),
]

handler404 = views.error_404
handler500 = views.error_500