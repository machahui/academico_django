from django.contrib.auth.hashers import PBKDF2PasswordHasher
import hashlib

class MyPBKDF2PasswordHasher(PBKDF2PasswordHasher):
    digest = hashlib.sha512
    algorithm = "pbkdf2_sha512"
    iterations = 19000
